package pl.com.bottega.na.examples.cart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
class CreateDocumentRequest extends EnvelopeRequest {

    public static final String ACTION = "CreateDocumentRequest";

    private Long id;
    private String number;
    private List<Long> verificationManagers = new ArrayList<>();
}
