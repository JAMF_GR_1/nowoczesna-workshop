package pl.com.bottega.na.examples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImproveRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImproveRestApplication.class, args);
	}
}
