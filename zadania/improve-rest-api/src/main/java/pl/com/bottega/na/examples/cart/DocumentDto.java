package pl.com.bottega.na.examples.cart;

import lombok.Data;

import java.util.List;

@Data
class DocumentDto {

    private Long id;
    private String number;
    private boolean verified;
    private List<Long> verificationManagers;

    DocumentDto(Long id, String number, List<Long> verificationManagers) {
        this.id = id;
        this.number = number;
        this.verificationManagers = verificationManagers;
    }
}
