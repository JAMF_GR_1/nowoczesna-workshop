package pl.com.bottega.na.examples.cart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
class VerifyDocumentRequest extends EnvelopeRequest {

    public static final String ACTION = "VerifyDocumentRequest";

    private Long id;
}
