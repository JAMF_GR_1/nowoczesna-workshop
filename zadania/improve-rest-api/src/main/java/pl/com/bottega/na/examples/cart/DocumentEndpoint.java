package pl.com.bottega.na.examples.cart;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
@Slf4j
class DocumentEndpoint {

    private List<DocumentDto> documents = new CopyOnWriteArrayList<>();

    @PostMapping
    ResponseEntity<Object> execute(@RequestBody EnvelopeRequest request) {
        if (request instanceof CreateDocumentRequest) {
            return createDocument((CreateDocumentRequest) request);
        } else if (request instanceof FindDocumentsRequest) {
            return findDocuments((FindDocumentsRequest) request);
        } else if (request instanceof VerifyDocumentRequest) {
            return verifyDocuments((VerifyDocumentRequest) request);
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    private ResponseEntity<Object> createDocument(CreateDocumentRequest request) {
        documents.add(new DocumentDto(
                request.getId(),
                request.getNumber(),
                request.getVerificationManagers()
        ));
        return ResponseEntity.ok().build();
    }

    private ResponseEntity<Object> findDocuments(FindDocumentsRequest request) {
        return ResponseEntity.ok(new FindDocumentsResponse(
                documents.stream()
                        .filter(documentDto -> documentDto.isVerified() == request.isVerified()
                                && documentDto.getVerificationManagers().contains(request.getVerificationManagerId())
                        ).collect(Collectors.toList())
        ));
    }

    private ResponseEntity<Object> verifyDocuments(VerifyDocumentRequest request) {
        documents.stream()
                .filter(documentDto -> documentDto.getId().equals(request.getId()))
                .findFirst().
                ifPresent(documentDto -> documentDto.setVerified(true));
        return ResponseEntity.ok().build();
    }
}
