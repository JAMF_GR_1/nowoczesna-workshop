package pl.com.bottega.na.examples.cart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
class FindDocumentsResponse {

    private List<DocumentDto> documents = new ArrayList<>();
}
