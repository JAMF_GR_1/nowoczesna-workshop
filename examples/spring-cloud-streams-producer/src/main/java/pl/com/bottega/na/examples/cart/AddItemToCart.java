package pl.com.bottega.na.examples.cart;

import lombok.Data;

@Data
class AddItemToCart {

    private Long itemId;
    private Long quantity;
}
