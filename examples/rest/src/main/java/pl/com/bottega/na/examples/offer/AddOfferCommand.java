package pl.com.bottega.na.examples.offer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
class AddOfferCommand {

    private final Long productId;
    private final BigDecimal price;

    @JsonCreator
    AddOfferCommand(@JsonProperty("productId") Long productId, @JsonProperty("price") BigDecimal price) {
        this.productId = productId;
        this.price = price;
    }
}
