package pl.com.bottega.na.examples.offer;

import lombok.Getter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
class Offer {

    private final Long productId;
    private final BigDecimal price;
    private final String id = UUID.randomUUID().toString();

    Offer(Long productId, BigDecimal price) {
        this.productId = productId;
        this.price = price;
    }

    boolean isPublished() {
        return false;
    }
}
