package pl.com.bottega.na.examples.offer;

import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

class OfferResource extends ResourceSupport {

    OfferResource(Offer content) {
        add(linkTo(methodOn(OfferEndpoint.class).getOffer(content.getId())).withSelfRel());
        if (!content.isPublished()) {
            add(linkTo(methodOn(OfferEndpoint.class).publish(content.getId())).withRel("publish"));
        }
    }
}
