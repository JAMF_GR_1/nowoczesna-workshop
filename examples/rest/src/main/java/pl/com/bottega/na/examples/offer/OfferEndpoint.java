package pl.com.bottega.na.examples.offer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.ResponseEntity.notFound;

@RestController
@RequestMapping("/offers")
class OfferEndpoint {

    private List<Offer> offers = new ArrayList<>();

    @PostMapping
    ResponseEntity<Offer> addOffer(@RequestBody AddOfferCommand cmd) {
        Offer offer = new Offer(cmd.getProductId(), cmd.getPrice());
        offers.add(offer);
        return ResponseEntity
                .created(linkTo(methodOn(OfferEndpoint.class).getOffer(offer.getId())).toUri())
                .contentType(APPLICATION_JSON)
                .body(offer);
    }

    @GetMapping("/{id}")
    ResponseEntity<Offer> getOffer(@PathVariable String id) {
        return offers.stream()
                .filter(o -> o.getId().equals(id))
                .findFirst()
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Void> publish(@PathVariable String id) {
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<OfferResource>> offers() {
        return ResponseEntity.ok(offers.stream()
                .map(OfferResource::new)
                .collect(Collectors.toList()));
    }
}
